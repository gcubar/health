﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Health.Models
{
    public class Hospital
    {
        [Key]
        public int HospitalID { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 2)]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [StringLength(250)]
        [Display(Name = "Descripción")]
        public string Description { get; set; }

        // collections
        [Display(Name = "Doctores")]
        public virtual ICollection<Doctor> Doctors { get; set; }

        [Display(Name = "Enfermeras")]
        public virtual ICollection<Nurse> Nurses { get; set; }

        [Display(Name = "Turnos de guardia")]
        public virtual ICollection<Turn> Turns { get; set; }

        [Display(Name = "Consultas")]
        public virtual ICollection<Consult> Consults { get; set; }
    }
}