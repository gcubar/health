﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Health.Models
{
    public class Pacient
    {
        [Key]
        public int PacientID { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 2)]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Required]
        [StringLength(120, MinimumLength = 2)]
        [Display(Name = "Apellidos")]
        public string LastName { get; set; }

        // collections
        [Display(Name = "Consultas")]
        public virtual ICollection<Consult> Consults { get; set; }
    }
}