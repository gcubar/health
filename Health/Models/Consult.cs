﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Health.Models
{
    public class Consult
    {
        [Key]
        public int ConsultID { get; set; }

        [Required(ErrorMessage = "Dato obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha y hora")]
        public DateTime ConsultDateTime { get; set; }

        public int DoctorID { get; set; }
        public int PacientID { get; set; }

        [Display(Name = "Doctor")]
        public virtual Doctor Doctor { get; set; }

        [Display(Name = "Paciente")]
        public virtual Pacient Pacient { get; set; }
    }
}