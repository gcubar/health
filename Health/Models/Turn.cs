﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Health.Models
{
    public class Turn
    {
        [Key]
        public int TurnID { get; set; }

        [Required(ErrorMessage = "Dato obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Turno de guardia")]
        public DateTime TurnDate { get; set; }

        public int DoctorID { get; set; }

        [Display(Name = "Doctor")]
        public virtual Doctor Doctor { get; set; }
    }
}