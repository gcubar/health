﻿using System;
using System.Collections.Generic;
using System.Linq;
using Health.Models;
using Health.DAL;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Health.ViewModels
{
    public class DoctorViewModel
    {
        private HealthContext db = new HealthContext();

        public DoctorViewModel(Doctor doctor)
        {
            this.Doctor = doctor;
            var pacients = GetAllPacients(doctor.DoctorID);

            this.Pacients = pacients.ToList();
        }

        public IQueryable<Pacient> GetAllPacients(int id)
        {
            return (from p in db.Pacients
                    from c in db.Consults
                    where c.DoctorID == id && p.PacientID == c.PacientID
                    select p).Distinct();
        }

        [Display(Name = "Doctor")]
        public Doctor Doctor { get; private set; }

        [Display(Name = "Pacientes")]
        public virtual ICollection<Pacient> Pacients { get; private set; }
    }
}