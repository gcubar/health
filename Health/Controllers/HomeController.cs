﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Health.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Inicio";
            ViewBag.Message = "Implementarme. Estoy en el Controlador Home, o sino, búscame por todo el proyecto.";

            return View();
        }

        public ActionResult Hospitals()
        {
            ViewBag.Message = "Hospitales.";

            return View();
        }

        public ActionResult Doctors()
        {
            ViewBag.Message = "Doctores.";

            return View();
        }

        public ActionResult Nurses()
        {
            ViewBag.Message = "Enfermeras.";

            return View();
        }

        public ActionResult Patients()
        {
            ViewBag.Message = "Pacientes.";

            return View();
        }

        public ActionResult Turns()
        {
            ViewBag.Message = "Turnos médicos.";

            return View();
        }

        public ActionResult Consults()
        {
            ViewBag.Message = "Consultas especializadas.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Title = "Acerca de nosotros";
            ViewBag.Message = "";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Title = "Contacto";
            ViewBag.Message = "";

            return View();
        }
    }
}
