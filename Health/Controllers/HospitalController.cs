﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using Health.Models;
using Health.DAL;

namespace Health.Controllers
{
    public class HospitalController : Controller
    {
        private HealthContext db = new HealthContext();

        //
        // GET: /Hospital/

        public ActionResult Index()
        {
            return View(db.Hospitals.ToList());
        }

        //
        // GET: /Hospital/Details/5

        public ActionResult Details(int? id = 0)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hospital hospital = db.Hospitals.Find(id);
            if (hospital == null)
            {
                return HttpNotFound();
            }
            return View(hospital);
        }

        //
        // GET: /Hospital/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Hospital/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, Description")]Hospital hospital)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Hospitals.Add(hospital);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Imposible guardar los cambios. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.");
            }

            return View(hospital);
        }

        //
        // GET: /Hospital/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Hospital hospital = db.Hospitals.Find(id);
            if (hospital == null)
            {
                return HttpNotFound();
            }
            return View(hospital);
        }

        //
        // POST: /Hospital/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HospitalID, Name, Description")]Hospital hospital)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(hospital).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Imposible guardar los cambios. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.");
            }

            return View(hospital);
        }

        //
        // GET: /Hospital/Delete/5

        public ActionResult Delete(int? id = 0, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Falló el borrado. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.";
            }
            Hospital hospital = db.Hospitals.Find(id);
            if (hospital == null)
            {
                return HttpNotFound();
            }
            return View(hospital);
        }

        //
        // POST: /Hospital/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Hospital hospital = db.Hospitals.Find(id);
                db.Hospitals.Remove(hospital);
                db.SaveChanges();

                /* 
                // another quickly form to delete item
                Hospital hospitalToDelete = new Hospital() { HospitalID = id };
                db.Entry(hospitalToDelete).State = EntityState.Deleted;
                 */
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}