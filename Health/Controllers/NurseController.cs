﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using Health.Models;
using Health.DAL;

namespace Health.Controllers
{
    public class NurseController : Controller
    {
        private HealthContext db = new HealthContext();

        //
        // GET: /Nurse/

        public ActionResult Index()
        {
            var nurses = db.Nurses.Include(n => n.Hospital);
            return View(nurses.ToList());
        }

        //
        // GET: /Nurse/Details/5

        public ActionResult Details(int? id = 0)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nurse nurse = db.Nurses.Find(id);
            if (nurse == null)
            {
                return HttpNotFound();
            }
            return View(nurse);
        }

        //
        // GET: /Nurse/Create

        public ActionResult Create()
        {
            ViewBag.HospitalID = new SelectList(db.Hospitals, "HospitalID", "Name");
            return View();
        }

        //
        // POST: /Nurse/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, LastName, HospitalID")]Nurse nurse)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Nurses.Add(nurse);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Imposible guardar los cambios. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.");
            }

            ViewBag.HospitalID = new SelectList(db.Hospitals, "HospitalID", "Name", nurse.HospitalID);
            return View(nurse);
        }

        //
        // GET: /Nurse/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Nurse nurse = db.Nurses.Find(id);
            if (nurse == null)
            {
                return HttpNotFound();
            }
            ViewBag.HospitalID = new SelectList(db.Hospitals, "HospitalID", "Name", nurse.HospitalID);
            return View(nurse);
        }

        //
        // POST: /Nurse/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "NurseID, Name, LastName, HospitalID")]Nurse nurse)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(nurse).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Imposible guardar los cambios. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.");
            }

            ViewBag.HospitalID = new SelectList(db.Hospitals, "HospitalID", "Name", nurse.HospitalID);
            return View(nurse);
        }

        //
        // GET: /Nurse/Delete/5

        public ActionResult Delete(int? id = 0, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Falló el borrado. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.";
            }
            Nurse nurse = db.Nurses.Find(id);
            if (nurse == null)
            {
                return HttpNotFound();
            }
            return View(nurse);
        }

        //
        // POST: /Nurse/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Nurse nurse = db.Nurses.Find(id);
                db.Nurses.Remove(nurse);
                db.SaveChanges();

                /* 
                // another quickly form to delete item
                Nurse nurseToDelete = new Nurse() { NurseID = id };
                db.Entry(nurseToDelete).State = EntityState.Deleted;
                 */
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}