﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using Health.Models;
using Health.DAL;

namespace Health.Controllers
{
    public class PacientController : Controller
    {
        private HealthContext db = new HealthContext();

        //
        // GET: /Pacient/

        public ActionResult Index()
        {
            return View(db.Pacients.ToList());
        }

        //
        // GET: /Pacient/Details/5

        public ActionResult Details(int? id = 0)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pacient pacient = db.Pacients.Find(id);
            if (pacient == null)
            {
                return HttpNotFound();
            }
            return View(pacient);
        }

        //
        // GET: /Pacient/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Pacient/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, LastName")]Pacient pacient)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Pacients.Add(pacient);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Imposible guardar los cambios. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.");
            }

            return View(pacient);
        }

        //
        // GET: /Pacient/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Pacient pacient = db.Pacients.Find(id);
            if (pacient == null)
            {
                return HttpNotFound();
            }
            return View(pacient);
        }

        //
        // POST: /Pacient/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PacientID, Name, LastName")]Pacient pacient)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(pacient).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Imposible guardar los cambios. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.");
            }

            return View(pacient);
        }

        //
        // GET: /Pacient/Delete/5

        public ActionResult Delete(int? id = 0, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Falló el borrado. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.";
            }
            Pacient pacient = db.Pacients.Find(id);
            if (pacient == null)
            {
                return HttpNotFound();
            }
            return View(pacient);
        }

        //
        // POST: /Pacient/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Pacient pacient = db.Pacients.Find(id);
                db.Pacients.Remove(pacient);
                db.SaveChanges();

                /* 
                // another quickly form to delete item
                Pacient pacientToDelete = new Pacient() { PacientID = id };
                db.Entry(pacientToDelete).State = EntityState.Deleted;
                 */
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}