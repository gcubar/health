﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using Health.Models;
using Health.DAL;

namespace Health.Controllers
{
    public class TurnController : Controller
    {
        private HealthContext db = new HealthContext();

        //
        // GET: /Turn/

        public ActionResult Index()
        {
            var turns = db.Turns.Include(t => t.Doctor);
            return View(turns.ToList());
        }

        //
        // GET: /Turn/Details/5

        public ActionResult Details(int? id = 0)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Turn turn = db.Turns.Find(id);
            if (turn == null)
            {
                return HttpNotFound();
            }
            return View(turn);
        }

        //
        // GET: /Turn/Create

        public ActionResult Create()
        {
            ViewBag.DoctorID = new SelectList(db.Doctors, "DoctorID", "Name");
            return View();
        }

        //
        // POST: /Turn/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TurnDate, DoctorID")]Turn turn)
        {
            try
            {
                // TODO: invalidate turns at the same date, how?
                if (ModelState.IsValid)
                {
                    db.Turns.Add(turn);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Imposible guardar los cambios. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.");
            }

            ViewBag.DoctorID = new SelectList(db.Doctors, "DoctorID", "Name", turn.DoctorID);
            return View(turn);
        }

        //
        // GET: /Turn/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Turn turn = db.Turns.Find(id);
            if (turn == null)
            {
                return HttpNotFound();
            }
            ViewBag.DoctorID = new SelectList(db.Doctors, "DoctorID", "Name", turn.DoctorID);
            return View(turn);
        }

        //
        // POST: /Turn/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TurnID, TurnDate, DoctorID")]Turn turn)
        {
            try
            {
                // TODO: invalidate turns at the same date, how?
                if (ModelState.IsValid)
                {
                    db.Entry(turn).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Imposible guardar los cambios. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.");
            }
            ViewBag.DoctorID = new SelectList(db.Doctors, "DoctorID", "Name", turn.DoctorID);
            return View(turn);
        }

        //
        // GET: /Turn/Delete/5

        public ActionResult Delete(int? id = 0, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Falló el borrado. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.";
            }
            Turn turn = db.Turns.Find(id);
            if (turn == null)
            {
                return HttpNotFound();
            }
            return View(turn);
        }

        //
        // POST: /Turn/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Turn turn = db.Turns.Find(id);
                db.Turns.Remove(turn);
                db.SaveChanges();

                /* 
                // another quickly form to delete item
                Turn turnToDelete = new Turn() { TurnID = id };
                db.Entry(turnToDelete).State = EntityState.Deleted;
                 */
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}