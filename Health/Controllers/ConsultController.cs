﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using Health.Models;
using Health.DAL;

namespace Health.Controllers
{
    public class ConsultController : Controller
    {
        private HealthContext db = new HealthContext();

        //
        // GET: /Consult/

        public ActionResult Index()
        {
            var consults = db.Consults.Include(c => c.Doctor).Include(c => c.Pacient);
            return View(consults.ToList());
        }

        //
        // GET: /Consult/Details/5

        public ActionResult Details(int? id = 0)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consult consult = db.Consults.Find(id);
            if (consult == null)
            {
                return HttpNotFound();
            }
            return View(consult);
        }

        //
        // GET: /Consult/Create

        public ActionResult Create()
        {
            ViewBag.DoctorID = new SelectList(db.Doctors, "DoctorID", "Name");
            ViewBag.PacientID = new SelectList(db.Pacients, "PacientID", "Name");
            return View();
        }

        //
        // POST: /Consult/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ConsultDateTime, DoctorID, PacientID")]Consult consult)
        {
            try
            {
                // TODO: invalidate consults at the same date & at the same turn day, how?
                if (ModelState.IsValid)
                {
                    db.Consults.Add(consult);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Imposible guardar los cambios. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.");
            }

            ViewBag.DoctorID = new SelectList(db.Doctors, "DoctorID", "Name", consult.DoctorID);
            ViewBag.PacientID = new SelectList(db.Pacients, "PacientID", "Name", consult.PacientID);
            return View(consult);
        }

        //
        // GET: /Consult/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Consult consult = db.Consults.Find(id);
            if (consult == null)
            {
                return HttpNotFound();
            }
            ViewBag.DoctorID = new SelectList(db.Doctors, "DoctorID", "Name", consult.DoctorID);
            ViewBag.PacientID = new SelectList(db.Pacients, "PacientID", "Name", consult.PacientID);
            return View(consult);
        }

        //
        // POST: /Consult/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ConsultID, ConsultDateTime, DoctorID, PacientID")]Consult consult)
        {
            try
            {
                // TODO: invalidate consults at the same date & at the same turn day, how?
                if (ModelState.IsValid)
                {
                    db.Entry(consult).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Imposible guardar los cambios. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.");
            }

            ViewBag.DoctorID = new SelectList(db.Doctors, "DoctorID", "Name", consult.DoctorID);
            ViewBag.PacientID = new SelectList(db.Pacients, "PacientID", "Name", consult.PacientID);
            return View(consult);
        }

        //
        // GET: /Consult/Delete/5

        public ActionResult Delete(int? id = 0, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Falló el borrado. Inténtalo de nuevo, y si el problema persiste contacta al administrador de sistema.";
            }
            Consult consult = db.Consults.Find(id);
            if (consult == null)
            {
                return HttpNotFound();
            }
            return View(consult);
        }

        //
        // POST: /Consult/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Consult consult = db.Consults.Find(id);
                db.Consults.Remove(consult);
                db.SaveChanges();

                /* 
                // another quickly form to delete item
                Consult consultToDelete = new Consult() { ConsultID = id };
                db.Entry(consultToDelete).State = EntityState.Deleted;
                 */
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}