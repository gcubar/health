﻿using Health.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Health.DAL
{
    public class HealthContext : DbContext
    {
        public HealthContext() : base("HealthContext") 
        { 
        }

        public DbSet<Hospital> Hospitals { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Nurse> Nurses { get; set; }
        public DbSet<Pacient> Pacients { get; set; }
        public DbSet<Turn> Turns { get; set; }
        public DbSet<Consult> Consults { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}