﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Health.Models;

namespace Health.DAL
{
    public class HealthInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<HealthContext>
    {
        protected override void Seed(HealthContext context)
        {
            var hospitals = new List<Hospital>
            {
                new Hospital{Name="Pedro del Toro", Description="Rpto. Pueblo Nuevo"},
                new Hospital{Name="Máximo Gómez", Description="Centro Ciudad"},
                new Hospital{Name="Vladimir I. Lenin", Description="Rpto. Lenin"},
                new Hospital{Name="Lucía Íñiguez", Description="Rpto. Villa Nueva"}
            };
            hospitals.ForEach(s => context.Hospitals.Add(s));
            context.SaveChanges();

            var doctors = new List<Doctor>
            {
                new Doctor{Name="Juan", LastName="Pérez Rodríguez", HospitalID=1},
                new Doctor{Name="Karina", LastName="Suárez Marrero", HospitalID=2},
                new Doctor{Name="Pedro", LastName="Hernández Álvarez", HospitalID=3},
                new Doctor{Name="Raúl", LastName="Armas Ramírez", HospitalID=4},
                new Doctor{Name="Yamila", LastName="Ricardo Menéndez", HospitalID=1},
                new Doctor{Name="María", LastName="Blanco Marrón", HospitalID=2},
                new Doctor{Name="Aimara", LastName="Mora Pérez", HospitalID=3},
                new Doctor{Name="Xavier", LastName="Castro Puyol", HospitalID=4},
            };
            doctors.ForEach(s => context.Doctors.Add(s));
            context.SaveChanges();

            var nurses = new List<Nurse>
            {
                new Nurse{Name="Yensy", LastName="Pérez Rodríguez", HospitalID=1},
                new Nurse{Name="Yusel", LastName="Suárez Marrero", HospitalID=2},
                new Nurse{Name="Yadira", LastName="Hernández Álvarez", HospitalID=3},
                new Nurse{Name="Yusimi", LastName="Armas Ramírez", HospitalID=4},
                new Nurse{Name="Yeny", LastName="Ricardo Menéndez", HospitalID=1},
                new Nurse{Name="Yanet", LastName="Blanco Marrón", HospitalID=2}
            };
            nurses.ForEach(s => context.Nurses.Add(s));
            context.SaveChanges();

            var pacients = new List<Pacient>
            {
                new Pacient{Name="Manuel", LastName="Pérez Rodríguez"},
                new Pacient{Name="Camila", LastName="Suárez Marrero"},
                new Pacient{Name="Raudelio", LastName="Hernández Álvarez"},
                new Pacient{Name="Jose", LastName="Armas Ramírez"},
                new Pacient{Name="Emilio", LastName="Ricardo Menéndez"},
                new Pacient{Name="Roberto", LastName="Blanco Marrón"},
                new Pacient{Name="Juan", LastName="Mora Pérez"},
                new Pacient{Name="Pedro", LastName="Castro Puyol"},
                new Pacient{Name="Alberto", LastName="Manduley Turruelles"},
                new Pacient{Name="Arianna", LastName="Pérez Sierra"},
                new Pacient{Name="Isabel", LastName="Garcés Martínez"},
                new Pacient{Name="Ricardo", LastName="Ricardo García"},
                new Pacient{Name="Miguel", LastName="García Céspedez"},
                new Pacient{Name="Carlos", LastName="Duvergel Mariño"}
            };
            pacients.ForEach(s => context.Pacients.Add(s));
            context.SaveChanges();

            var turns = new List<Turn>
            {
                new Turn{TurnDate=DateTime.Parse("2015-01-01"), DoctorID=1},
                new Turn{TurnDate=DateTime.Parse("2015-01-01"), DoctorID=2},
                new Turn{TurnDate=DateTime.Parse("2015-01-01"), DoctorID=3},
                new Turn{TurnDate=DateTime.Parse("2015-01-01"), DoctorID=4},
                new Turn{TurnDate=DateTime.Parse("2015-01-02"), DoctorID=5},
                new Turn{TurnDate=DateTime.Parse("2015-01-02"), DoctorID=6},
                new Turn{TurnDate=DateTime.Parse("2015-01-02"), DoctorID=7},
                new Turn{TurnDate=DateTime.Parse("2015-01-02"), DoctorID=8},
                new Turn{TurnDate=DateTime.Parse("2015-01-03"), DoctorID=1},
                new Turn{TurnDate=DateTime.Parse("2015-01-03"), DoctorID=2},
                new Turn{TurnDate=DateTime.Parse("2015-01-03"), DoctorID=3},
                new Turn{TurnDate=DateTime.Parse("2015-01-03"), DoctorID=4}
            };
            turns.ForEach(s => context.Turns.Add(s));
            context.SaveChanges();

            var consults = new List<Consult>
            {
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-01 08:00"), DoctorID=5, PacientID=1},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-01 09:00"), DoctorID=6, PacientID=2},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-01 10:00"), DoctorID=7, PacientID=3},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-01 11:00"), DoctorID=8, PacientID=4},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-02 08:00"), DoctorID=1, PacientID=5},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-02 09:00"), DoctorID=2, PacientID=6},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-02 10:00"), DoctorID=3, PacientID=7},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-02 11:00"), DoctorID=4, PacientID=8},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-01 08:00"), DoctorID=6, PacientID=9},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-01 09:00"), DoctorID=7, PacientID=10},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-01 10:00"), DoctorID=8, PacientID=11},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-02 08:00"), DoctorID=2, PacientID=12},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-02 09:00"), DoctorID=3, PacientID=13},
                new Consult{ConsultDateTime=DateTime.Parse("2015-01-02 10:00"), DoctorID=4, PacientID=14},
            };
            consults.ForEach(s => context.Consults.Add(s));
            context.SaveChanges();
        }
    }
}